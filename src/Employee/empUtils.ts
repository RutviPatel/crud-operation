import { resourceLimits } from "worker_threads";

const My = require('jm-ez-mysql');

export class EmpUtils{
    public createEmp = async(empData:JSON) => {
        // const data=await My.insert("Employee",empData);
        // return data.insertId;
        return await My.insert("Employee",empData)
    }

    public getAllData = async() => {
        return await My.findAll("Employee",['eid','name','age','city','clientId']);
    }

    public getAllDataOrder = async() => {
        // const where = `name = ?`;
        const sortQuery ='ORDER BY name ASC';
        const result = await My.findAll("Employee",['eid','name','age','city','clientId'],`1=1 ${sortQuery}`);
        return result;
    }


    public getAllOrder = async() => {
        const sortQuery ='ORDER BY city DESC';
        const result = await My.findAll("Employee",['eid','name','age','city','clientId'],`1=1 ${sortQuery}`);
        console.log(My.lQ);
        return result;
    }

    public updateData = async(id:Number,empData:JSON) => {
        return await My.update("Employee",empData,"eid=?",[id]);
    }

    public deleteData = async(id:Number) =>  {
        return await My.delete("Employee","eid=?",[id]);
    }

    public DeleteEmployeeAllService = async(id:number, clientId:number) => {
        return await My.delete("Employee",
        `eid = ? AND clientId = ?`, [id, clientId]);
    }

    public async employee(reqData: any) {
        const id = reqData.id;
        const where = `eid = ${id}`;
        const params = ["*"];
        const result = await My.first("Employee", params, `${where}`);
        console.log(My.lQ);
        return result;
        
    }


    public async empWithCount(reqData: any) {
        const name = reqData.name;
        console.log(name);
        
        const where = `name = ?`;
        const count = ["name"];
        const params = ["*"];
        const result= await My.first("Employee",["COUNT(DISTINCT eid) as Count"],`${where}`,[name]);
        console.log(My.lQ);
        return result;
    }


    public async addManyEmployee(reqData: any){
        const name = reqData.name;
        const age = reqData.age;
        const city = reqData.city;
        const clientId = reqData.clientId;

        const result = await My.insertMany("Employee",[
            {
                name: "Jeh",
                age: 24,
                city: "Jaipur",
                clientId:399
            },
            {
                name: "Tax",
                age: 18,
                city: "Valsad",
                clientId: 422
            },
        ]);
        console.log(My.lQ);
        return result;
    }

    public async updateFirstEmployee(reqData: JSON, reqParData: any){
        const id = reqParData.id;
        const where = `eid = ?`;
        const result = await My.updateFirst("Employee",reqData,`${where}`, id);
        console.log(My.lQ);
        return result;
    }

    public joinQuery = async() => {
        const selectParams = ["Employee.name", "Employee.age", "Client2.email", "Client2.e_id"];
        const query = `Employee inner join Client2 on Employee.eid=Client2.e_id`;
        const result = await My.findAll(`${query}`,selectParams);
        console.log(My.lQ);
        return result;
    }

    public leftJoinQuery = async() => {
        const selectParams = ["Employee.name", "Employee.age", "Client2.email", "Client2.e_id"];
        const query = `Employee left join Client2 on Employee.eid=Client2.e_id`;
        const result = await My.findAll(`${query}`,selectParams);
        console.log(My.lQ);
        return result;
    }

    public rightJoinQuery = async() => {
        const selectParams =["Employee.name", "Employee.city", "Client2.email", "Client2.e_id"];
        const query = `Employee right join Client2 on Employee.eid=Client2.e_id`;
        const result = await My.findAll(`${query}`,selectParams);
        console.log(My.lQ);
        return result;
    }

    public crossJoinQuery = async() => {
        const selectParams = ["Employee.city", "Employee.age", "Client2.name", "Client2.email"];
        const query = `Employee CROSS JOIN Client2`;
        const result = await My.findAll(`${query}`,selectParams);
        console.log(My.lQ);
        return result;
    }

    public selfJoinQuery = async() => {
        const selectParams = ["A.name AS name1", "B.name AS name2", "A.city"];
        const query = `Employee A , Employee B`;
        let where = `(A.eid <> B.eid) AND (A.city = B.city)`;
        where += `ORDER BY A.city`;
        const result = await My.findAll(`${query}`, selectParams , `${where}`);
        console.log(My.lQ);
        return result;
        
    }

      public checkClientDetails = async() => {
        // const where = `Client2.e_id AND Employee.clientId`;
        const selectParams = ["Client2.email", "Employee.eid", "Employee.name", "Client2.name"];
        const joinQuery = `Client2 LEFT JOIN Employee  ON Employee.name = Client2.name`;
         return await My.first(`${joinQuery}`, selectParams);
    }

    public createClient = async(empData:JSON) => {
        return await My.insert("Client2", empData);
    }

    public groupByQuery = async() => {
        const query = `GROUP BY name `;
        const result = await My.findAll("Client2", "COUNT(e_id), name", `1=1 ${query}`);
        console.log(My.lQ);
        return result;
    }

    public countQuery = async() => {
        const result = await My.findAll("Employee", "COUNT(city)");
        return result;
    }

    public count = async() => {
        const where = `city = "PATAN"`;
        const result = await My.findAll("Employee", "COUNT(city)", `${where}`);
        return result;
    }

    public like = async() => {
        const where = `name like '%i'`;
        const Params = ["*"];
        const result = await My.findAll("Employee", Params, `${where}`);
        return result;
    }
     
    public inQuery = async() => {
        const where = `age IN (23, 24, 25, 7)`;
        const Params = ["*"];
        const result = await My.findAll("Employee", Params, `${where}`);
        return result;
    }

    public notInQuery = async() => {
        const where = `age NOT IN (23, 24, 25, 7, 18)`;
        const Params = ["*"];
        const result = await My.findAll("Employee", Params, `${where}`);
        return result;
    }
}
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HomeRoute = void 0;
const express_1 = require("express");
const homeController_1 = require("./homeController");
const router = (0, express_1.Router)();
//route controller init
const homeController = new homeController_1.HomeController();
router.get("/pug", homeController.homeController);
exports.HomeRoute = router;

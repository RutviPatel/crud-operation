import {Router} from "express";
import { EmpController } from "./empController";

const router:Router = Router();
const ec = new EmpController();

router.get('/', ec.getEmp);
router.get('/sort', ec.getEmpSort);
router.get('/desc', ec.getSort);
router.get('/emp/:id', ec.employee);
router.get("/empWithCount", ec.empWithCount);
router.post('/addEmp', ec.addEmp);
router.post('/addManyEmployee', ec.addManyEmployee);
router.post('/update/:id', ec.updateEmp);
router.post('/updateFirstEmployee/:id', ec.updateFirstEmployee);
router.delete('/deleteEmp/:id', ec.deleteEmp);
router.delete('/:clientId/Delete-client-Id/:id', ec.DeleteEmployeeAllService);
router.get('/join', ec.joinQuery);
router.get('/leftjoin', ec.leftJoinQuery);
router.get('/rightjoin', ec.rightJoinQuery);
router.get('/crossjoin', ec.crossJoinQuery);
router.post('/addcli', ec.createClient);
router.get('/selfjoin', ec.selfJoinQuery);
router.get('/client', ec.checkClientDetails);
router.get('/groupby', ec.groupByQuery);
router.get('/count', ec.countQuery);
router.get('/countc', ec.count);
router.get('/like', ec.like);
router.get('/in', ec.inQuery);
router.get('/notin', ec.notInQuery);


export const empRoute:Router=router;

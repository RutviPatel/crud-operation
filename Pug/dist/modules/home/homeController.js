"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HomeController = void 0;
const homeUtils_1 = require("./homeUtils");
const pug_1 = __importDefault(require("pug"));
const path_1 = __importDefault(require("path"));
class HomeController {
    constructor() {
        this.home = new homeUtils_1.HomeUtils();
        this.homeController = (req, res) => {
            // const result = this.home.home();
            const StaticFiles = path_1.default.join(__dirname, '../../../src/static');
            const html = pug_1.default.renderFile(`${StaticFiles}/pug/index.pug`);
            res.status(200).send(html);
        };
    }
}
exports.HomeController = HomeController;

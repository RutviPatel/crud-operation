"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = void 0;
const my = require('jm-ez-mysql');
class DB {
    static init() {
        my.init({
            acquireTimeout: 100 * 60 * 1000,
            connectTimeout: 100 * 60 * 1000,
            connectionLimit: 10000,
            database: process.env.DATABASE,
            dateStrings: true,
            host: process.env.DBHOST,
            password: process.env.DBPASSWORD,
            timeout: 100 * 60 * 1000,
            timezone: "utc",
            user: process.env.DBUSER,
        });
    }
}
exports.DB = DB;

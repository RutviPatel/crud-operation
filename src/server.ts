import express  from 'express'
import * as bodyParser from "body-parser";
import * as dotenv from "dotenv";
import { RoutesPath } from "./routes";
import { DB } from './database'
const upload=require('express-fileupload');
dotenv.config();
DB.init();


class App{
    protected app:express.Application;
    constructor(){
       this.app=express();
       const PORT=process.env.PORT;
       this.app.use(upload());
       const rp = new RoutesPath();
       this.app.use(express.json());
       this.app.use("/api",rp.path()); 
       const Server = this.app.listen(PORT, () => {
        console.log(`The server is running in port localhost: ${PORT}`);
        });
    }
}
new App();

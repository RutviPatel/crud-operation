import * as express from 'express';
import { HomeRoute } from './modules/home/homeRouter';

export class Routes {
    public path() {
        const router = express.Router();
        router.use('/home', HomeRoute);
        router.all('/*', (req, res) => {
            return res.status(404).json({
                error: 'ERR_URL_NOT_FOUND',
            });
        });
        return router;
    }
}

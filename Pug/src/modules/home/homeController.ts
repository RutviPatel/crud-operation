import { Request , Response } from 'express'
import { HomeUtils } from './homeUtils';
import  pug  from 'pug';
import path from 'path';


export class HomeController{
    private home : HomeUtils = new HomeUtils(); 
    public homeController = (req : Request,res : Response) =>{
        // const result = this.home.home();
        const StaticFiles = path.join(__dirname, '../../../src/static');
        const html = pug.renderFile(`${StaticFiles}/pug/index.pug`);


        res.status(200).send(html);
    }
}
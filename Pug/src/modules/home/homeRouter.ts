import { Router } from "express";
import { HomeController } from './homeController'

const router: Router = Router();

//route controller init
const homeController = new HomeController();

router.get("/pug", homeController.homeController);


export const HomeRoute : Router = router;

import { resourceLimits } from "worker_threads";
import path = require("path");
import { Excel } from "./report";
const My = require('jm-ez-mysql');
const XLSX = require("xlsx");
const express = require("express");
const excel = require("exceljs");

const nodemailer = require("nodemailer");
const transport = nodemailer.createTransport({
    host:'smtp.gmail.com',
    secure:false,
    requireTLS:true,
    auth:{
        user:'rutvi.patel1.sa@gmail.com',
        pass:'Write some content'
    }
})
const mailOptions = {
    from:'rutvi.patel1.sa@gmail.com',
    to:'rutvi1073@gmail.com',
    subject:'Enviornment',
    text:'Hello ......................'
}


    export class taskUtils{
        private excel: Excel = new Excel();
        public async addTask(reqData: any) {
        const TID = reqData.TID;
        const Name = reqData.Name;
        const Date = reqData.Date;
        const Description = reqData.Description;

    const result = await My.insert(`task`, reqData);    
    }


    //delete user
    public async deleteTask(TID: any) {
        const where = `TID = ${TID}`;
        const selectParams = ['TID, first_name, task_status, id'];
        const result = await My.findAll('subtask', selectParams, `${where}`);
        const result2 = await My.delete("task", `${where}`);
        console.log(My.lQ);
        
        if(result.length === 0){
            return "there are no refernce in sub task"
        }else{
            return "task deleted"
        }
    }

    //get all details
    public async getAllData() {
        const selectParams = ['t.TID','id','Name','Date','First_name', 'Task_status','Description']
        const joinQuery = `task as t LEFT JOIN subtask as st on t.TID = st.TID`;
        const result = await My.findAll(`${joinQuery}`, selectParams);
        console.log(My.lQ);
        return result;
    }
    
    
    //get all details for specific user task
    public async getTaskDetails(TID: any){
        const selectParams = ['t.TID','id','Name','Date','First_name', 'Task_status','Description']
        const joinQuery = `task as t LEFT JOIN subtask as st on t.TID = st.TID`;
        const where = `st.TID = ${TID}`;
        const result = await My.findAll(`${joinQuery}`, selectParams, `${where}`);
        console.log(My.lQ);
        return result; 
        
    }

    //update task with TID
    public async updateTask(reqData: any){
        const TID = reqData.TID;
        const id = reqData.id;
        const First_name = reqData.First_name;
        const Task_status = reqData.Task_status;
        const where = `id = ${id} and TID = ${TID}`;

        const result = await My.update(`subtask`, reqData, `${where}`); 
        console.log(My.lQ);
        return result;
        
    }


    //UPLOAD FILE
    public async uploadImage(files: any, params: any) {
        try {
        const id = params.id;
        console.log(id,"++++++++++++++++++++++++++++++++=====");
                
        const file = files.file;
        const fileName = file.Name;

        //SET PATH FOR IMAGE
        const publicPath = path.join(__dirname, '../images/images');
        const URL = publicPath + " " + fileName;
               
        file.mv(URL, async(err: any) => {
           const result = await My.insert('image', { id : id , image : file + fileName});
        });
        return "File successfully uploaded";
        } 
        catch (err) {
        ({ message: "generate error" });
        }  
    }


    //UPLOAD MULTIPLE FILE
    public async uploadMultipleImage(files: any, params: any) {
        if (files) {
            const id = params.id;
            const file = files.file;
            const fileName = file.Name;
            const publicPath = path.join(__dirname, '../images/images');
            for (let i = 0; i < file.length; i++) {
                
                const URL = publicPath + " "  + file[i].Name;
                file[i].mv(URL, async (err: any) => {
                const result = await My.insert('image', { id: id, image: file[i] + file[i].Name});
                console.log(My.lQ);
                });
            }
            return "files successfully uploaded";
        } else {
            return "generate error";
        }
    }



    //Bulk upload
    public async addExcelReport(files: any) {
            const publicPath = path.join(__dirname, '/');
            if (files) {
                const file = files.file;
                const extension = path.extname(file.name);
                console.log(file,"----------------------------------------------");
                
                if (extension != ".xlsx") {
                    throw "Unsupported extension!";
                } else {
                    const URL = publicPath + ' ' + file.name;
                    const data = await file.mv(URL).then(async () => {
                        const res = await this.uploadData(URL);
                        return res;
                    });
                    return data;
                }
            } else {
                return "no file available";
            }
    }

    public async uploadData(file: any) {
        try {
            const excelData = await XLSX.readFile(file);
            const sheet = excelData.SheetNames;
            const  data: any[] = [];

            for (let i = 0; i < sheet.length; i++) {
                const sheetName = sheet[i];
                const sheetData = XLSX.utils.sheet_to_json(
                // console.log(sheetData,"iuwesojfdvvvvvvvvvvvvvvvvvvvvvvvcoki");
                    
                excelData.Sheets[sheetName]);
                sheetData.forEach((ele: any) => {
                    data.push(ele);
                });
                
                try{
                const result = await My.insertMany('task',data);
                // console.log(result,"-----------**********----------");
                if(result){
                    return "Data saved successfully."
                }
                else{
                    return "Data not saved."
                }
            }catch(err){
                return "Invalid Format"
            }
            }
        } catch (err) {
            return err;
        }
    }


    //bulk upload 
    public async addReport(file: any) {
        try {
            const publicPath = path.join(__dirname);
            const URL = publicPath + "/ExcelFiles/t3.xlsx";
            const excelData = await XLSX.readFile(URL);
            const sheet = excelData.SheetNames;
            console.log("count = ",sheet.length);
            
            const data: any[] = [];
            for (let i = 0; i < sheet.length; i++) {
                const sheetName = sheet[i];
                const sheetData = XLSX.utils.sheet_to_json(
                    excelData.Sheets[sheetName]
                );
                // console.log(sheetData+"jhggggggnjhhh-------")
                sheetData.forEach((ele:any) => {
                    data.push(ele);
                });
            }
            // console.log(data);
        try{    
        const result = await My.insertMany('task',data);
        if(result){
            return "Data saved successfully."
        }
        else{
            return "Data not saved."
        }
        }catch(err){
            return "Invalid Format"
        }
        }catch (err) {
            return err;
        }
    }


    //export excel report using excel
    public async generateTaskReport(TID: number) {
            const selectParams = ["*"];
            const where = "t.TID = 140";
            const joinQuery = `task as t LEFT JOIN subtask as st ON t.TID = st.TID`;
            const data = await My.findAll(`${joinQuery}`, selectParams, `${where}`);
            const res = this.excel.excelFile(data);
            console.log(My.lQ);
            return data;
        }
        
        
        
        
    //export excel report using exceljs
        public async generateReport(){
        const selectParams = ["*"];
        const where = "t.TID = 140";
        const joinQuery = `task as t LEFT JOIN subtask as st ON t.TID = st.TID`;
        try{
            const all = await My.findAll(`${joinQuery}`, selectParams, `${where}`);
             console.log(all);
             const workbook=new excel.Workbook();
             const worksheet = workbook.addWorksheet('task');
             
             worksheet.columns=[
                 {header:'TID',key:'TID',width:10},
                 {header:'Name',key:'Name',width:20},
                 {header:'Date',key:'Date',width:20},
                 {header:'Description',key:'Description',width:20},
                 {header:'id',key:'id',width:20},
                 {header:'First_name',key: 'First_name',width:20},
                 {header:'Task_status',key:'Task_status',width:20}
             ];
             
             all.forEach((user:any)=>{
                 worksheet.addRow(user);
             })

             worksheet.getRow(1).eachCell((cell: any)=>{
                 cell.font={bold:true};
             })
                  
             const data=await workbook.xlsx.writeFile('users.xlsx');
        } catch(err){
            console.log(err)
        }
 }
  


    //export excel report using exceljs
    public async generateExcelReport(id: number){
        const selectParams = ["*"];
        const where = `id = ${id}`;
        try{
             const r2=await My.findAll('image', selectParams, `${where}`);
             console.log(r2);
             const workbook=new excel.Workbook();
             const worksheet = workbook.addWorksheet('image');
             
             worksheet.columns=[
                 {header:'id',key:'id',width:10},
                 {header:'image',key:'image',width:20}
             ];
             
             r2.forEach((user:any)=>{
                 worksheet.addRow(user);
             })

             worksheet.getRow(1).eachCell((cell: any)=>{
                 cell.font={bold:true};
             })
                  
             const data=await workbook.xlsx.writeFile('images.xlsx');
        } catch(err){
            console.log(err)
        }
}


    //send Mail
    public async sendMail() {
    transport.sendMail(mailOptions,function(error: any, info: any){
        if(error){
            console.log(error);
        }else{
            console.log("email has been sent",info.response);
        }  
    });
    }
}


   



import dotenv from "dotenv";
const my=require('jm-ez-mysql');


export class DB {
    public static init() {
        console.log("connection successfully.");
        my.init({
            database: process.env.DATABASE,
            host: process.env.DBHOST,
            password: process.env.DBPASSWORD,
            user: process.env.DBUSER,
        });
        
    }
}

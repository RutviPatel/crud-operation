import xlsx from "xlsx";
// import ExcelJS from "exceljs";

export class Excel {
    public async excelFile(data: any) {
        const worksheet = xlsx.utils.json_to_sheet(data);
        const workbook = xlsx.utils.book_new();

        xlsx.utils.book_append_sheet(workbook, worksheet, "data");

        const xls = xlsx.write(workbook, { bookType: "xlsx", type: "buffer" });

        return xls;
    }
}
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutesPath = void 0;
const express_1 = __importDefault(require("express"));
const empRoute_1 = require("./a1/empRoute");
class RoutesPath {
    path() {
        const router = express_1.default.Router();
        router.use("/emp", empRoute_1.empRoute);
        return router;
    }
}
exports.RoutesPath = RoutesPath;

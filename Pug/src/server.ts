import express from 'express';
import dotenv from 'dotenv';
import { Routes } from './router';
import path from 'path';
dotenv.config();

export class App {
    protected app: express.Application;
    constructor() {
        const PORT = process.env.PORT;
        const StaticFiles = path.join(__dirname, '../src/static');
        this.app = express();
        this.app.all('/*', (req, res, next) => {
            res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE');
            if (req.method === 'OPTIONS') {
                res.writeHead(200);
                res.end();
            } else {
                next();
            }
        });
        //views
        // this.app.set('view engine', 'ejs');
        // this.app.set('views', publicPath + '/templates/views');

        //static files
        this.app.use('/static', express.static(StaticFiles));
        
        //urlencoding & json
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(express.json({ limit: '10mb' }));
        this.app.use(
            express.json(),
            (error: any, req: any, res: express.Response, next: any) => {
                if (error) {
                    return res
                        .status(400)
                        .json({ error: req.t('ERR_GENERIC_SYNTAX') });
                }
                next();
            }
        );

        const routes = new Routes();
        this.app.use('/', routes.path());

        const Server = this.app.listen(PORT, () => {
            console.log(
                `The server is running in port localhost: ${process.env.PORT}`
            );

            this.app.use((err: any, req: any, res: any, next: () => void) => {
                if (err) {
                    console.log(`ERROR : ${err}`);
                    res.status(500).json({
                        error: 'ERR_INTERNAL_SERVER',
                    });
                    return;
                }
                next();
            });
        });

    }
}

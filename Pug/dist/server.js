"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const router_1 = require("./router");
const path_1 = __importDefault(require("path"));
dotenv_1.default.config();
class App {
    constructor() {
        const PORT = process.env.PORT;
        const StaticFiles = path_1.default.join(__dirname, '../src/static');
        this.app = (0, express_1.default)();
        this.app.all('/*', (req, res, next) => {
            res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE');
            if (req.method === 'OPTIONS') {
                res.writeHead(200);
                res.end();
            }
            else {
                next();
            }
        });
        //views
        // this.app.set('view engine', 'ejs');
        // this.app.set('views', publicPath + '/templates/views');
        //static files
        this.app.use('/static', express_1.default.static(StaticFiles));
        //urlencoding & json
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(express_1.default.json({ limit: '10mb' }));
        this.app.use(express_1.default.json(), (error, req, res, next) => {
            if (error) {
                return res
                    .status(400)
                    .json({ error: req.t('ERR_GENERIC_SYNTAX') });
            }
            next();
        });
        const routes = new router_1.Routes();
        this.app.use('/', routes.path());
        const Server = this.app.listen(PORT, () => {
            console.log(`The server is running in port localhost: ${process.env.PORT}`);
            this.app.use((err, req, res, next) => {
                if (err) {
                    console.log(`ERROR : ${err}`);
                    res.status(500).json({
                        error: 'ERR_INTERNAL_SERVER',
                    });
                    return;
                }
                next();
            });
        });
    }
}
exports.App = App;

import {Response,Request} from 'express';
const My = require('jm-ez-mysql');
import  {EmpUtils} from './empUtils';

export class EmpController{
    private empUtils:EmpUtils = new EmpUtils();

    public addEmp = async(req:Request,res:Response) => {
        const savedData = await this.empUtils.createEmp(req.body);
        if(savedData){
            return res.json({success:"Data saved"});
        }
        else{
            return res.json({failed:"data not saved"});
        }
    }


    public getEmp = async(req:Request, res:Response) => {
        const allData = await this.empUtils.getAllData();
        return res.json({"All Employee Data":allData});
    }

    public getEmpSort = async(req:Request, res:Response) => {
        const allData = await this.empUtils.getAllDataOrder();
        return res.json({"All Employee Data":allData});
    }

    public getSort = async(req:Request, res:Response) => {
        const allData = await this.empUtils.getAllOrder();
        return res.json({"All Employee Data":allData});
    }

    public updateEmp = async(req:Request, res:Response) => {
        const id = req.params.id;
        const sid = Number(id);
        const updateRecord = await this.empUtils.updateData(sid,req.body);
        if(updateRecord){
            return res.json({success:"Data updated successfully."});
        }
        else{
            return res.json({success:"Record not update"});
        }
    }

    public deleteEmp = async(req:Request, res:Response) => {
        const id = req.params.id;
        const sid = Number(id);
        // console.log(id);
        // res.send("delete Request."); 
        const deleteRecord=await this.empUtils.deleteData(sid);
        if(deleteRecord){
            return res.json({success:"Data Deleted successfully."});
        }
        else{
            return res.json({failed:"Record not update"});
        }  
    }


    public DeleteEmployeeAllService = async(req:any, res: Response) => {
        const result = await this.empUtils.DeleteEmployeeAllService(
            +req.params.id, +req.params.clientId);
            res.json({success: "Employee All Service Deleted"});
    }

    public employee = async (req: any, res: Response) => {
        const reqData = req.params;
        const result = await this.empUtils.employee(reqData);
         res.status(200).json({Data: result});
    }


    public empWithCount = async (req: any, res: Response) => {
        const reqData = req.body;
        const result = await this.empUtils.empWithCount(reqData);
        res.status(200).json({ data: result });
    }

    public addManyEmployee = async (req:any, res: Response) => {
        const reqData = req.body;
        const result = await this.empUtils.addManyEmployee(reqData);
        res.status(200).json({ data: result});
    }

    public updateFirstEmployee = async(req: any, res: Response) => {
        const reqData = req.body;
        const reqParData = req.params;
        const result = await this.empUtils.updateFirstEmployee(reqData, reqParData);
        res.status(200).json({ data: result });
    }

    public checkClientDetails = async(req: any, res: Response) => {
        const result = await this.empUtils.checkClientDetails();
        res.status(200).json({ data: result});
    }


    public joinQuery = async(req: Request, res: Response) => {
        const result = await this.empUtils.joinQuery();
        res.status(200).json({data: result});
     }

     public leftJoinQuery = async(req:Request, res:Response) => {
        const result = await this.empUtils.leftJoinQuery();
        res.status(200).json({data:result});
     }

     public rightJoinQuery = async(req:Request, res:Response) => {
        const result = await this.empUtils.rightJoinQuery();
        res.status(200).json({data:result});
     }

     public crossJoinQuery = async(req: Request, res: Response) => {
         const result = await this.empUtils.crossJoinQuery();
         res.status(200).json({data:result});
     }

     public selfJoinQuery = async(req: Request, res: Response) => {
         const result = await this.empUtils.selfJoinQuery();
         res.status(200).json({data:result});
     }

     public createClient = async(req: Request, res: Response) => {
        const result = await this.empUtils.createClient(req.body);
        if(result){
            return res.json({success:"Data saved"});
        }
        else{
            return res.json({failed:"data not saved"});
        }
     }

     public groupByQuery = async(req: Request, res: Response) => {
          const result = await this.empUtils.groupByQuery();
          res.status(200).json({data:result});
     }

     public countQuery = async(req: Request, res: Response) => {
            const result = await this.empUtils.countQuery();
            res.status(200).json({data:result});
     }


     public count = async(req: Request, res: Response) => {
        const result = await this.empUtils.count();
        res.status(200).json({data:result});
     }

     public like = async(req: Request, res: Response) => {
         const result = await this.empUtils.like();
         res.status(200).json({data:result});
     }

     public inQuery = async(req: Request, res: Response) => {
        const result = await this.empUtils.inQuery();
        res.status(200).json({data:result});
    }

    public notInQuery = async(req: Request, res: Response) => {
        const result = await this.empUtils.notInQuery();
        res.status(200).json({data:result});
    }
}
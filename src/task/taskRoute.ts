import {Router} from "express";
import { taskController } from "./taskController";

const router:Router = Router();
const ec = new taskController();

router.post('/addtask',ec.addtask);
router.delete('/deletetask/:TID',ec.deletetask);
router.get('/getDetail',ec.getData);
router.get('/getAllInfo/:TID',ec.getTaskDetails);
router.put('/:id/:TID',ec.updateTask);
router.post('/upload/:id',ec.uploadImage);
router.post('/multipleUpd/:id',ec.uploadMultipleImage);
router.post('/uploadexcel',ec.addExcelReport);
router.post('/report',ec.addReport);
router.post('/excel/:id',ec.generateTaskReport);
router.get('/generate/',ec.generateReport);
router.post('/exceljs/:id',ec.genrateExceljsReport);
router.get('/mail',ec.sendMail);




export const taskRoute:Router=router;
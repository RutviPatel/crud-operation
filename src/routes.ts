import express from "express";
import { empRoute } from './Employee/empRoute';
import { taskRoute } from "./task/taskRoute";
export class RoutesPath{
    public path(){
        const router = express.Router();
        router.use("/emp",empRoute);
        router.use("/task",taskRoute);
        return router;
    }
}
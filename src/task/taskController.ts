import {Response,Request} from 'express';
const My = require('jm-ez-mysql');
import path = require("path");
const XLSX = require("xlsx");
var _ = require('lodash');

import {taskUtils} from './taskUtils';


export class taskController{
    private taskUtils:taskUtils = new taskUtils();
    private dateUtils = new Date();

    public addtask = async(req:Request,res:Response) => {
        const result = await this.taskUtils.addTask(req.body);
        res.status(200).json({ data: result });
    }

    public deletetask = async(req:Request,res:Response) => {
        const result = await this.taskUtils.deleteTask(req.params.TID);
        res.status(200).json({ data: result });
        
    }

    public getData =async (req: Request, res:Response) => {
        const result = await this.taskUtils.getAllData();
        res.status(200).json({ data: result });
    }

    public getTaskDetails =async (req: Request, res:Response) => {
        const result = await this.taskUtils.getTaskDetails(req.params.TID);
        res.status(200).json({ data: result });
    }

    public updateTask = async(req:Request,res:Response) => {
        const result = await this.taskUtils.updateTask(req.body);
        res.status(200).json({ data: result });
    }


    public uploadImage = async (req: any, res: Response) => {
        const result :any= await this.taskUtils.uploadImage(req.files, req.params);
        console.log(req.params,"dwarzsdxxxxxxxxxxxxxxxxxxxxxxxxxx");
        res.status(200).json({ data: result });
    }

    public uploadMultipleImage = async (req: any, res: Response) => {
        const result :any= await this.taskUtils.uploadMultipleImage(req.files, req.params);
        res.status(200).json({ data: result });
    }

    public addExcelReport = async(req: any, res: Response) => {
        const result = await this.taskUtils.addExcelReport(req.files);
        res.status(200).json({ data: result });
    };
    

    public addReport = async(req: any, res: Response) => {
        const result = await this.taskUtils.addReport(req.files);
        res.status(200).json({ data: result });
    };

   
    public generateTaskReport = async(req: any, res: Response) => {
        const result = await this.taskUtils.generateTaskReport(+req.params.id);
        res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        res.status(200).send(result);
    };


    public generateReport = async(req: any, res: Response) => {
        const result = await this.taskUtils.generateReport();
        res.status(200).send(result);
    };


    public genrateExceljsReport = async(req: any, res: Response) => {
        const result = await this.taskUtils.generateExcelReport(+req.params.id);
        res.status(200).send(result);
    };

    public sendMail = async(req: any, res: Response) => {
        const result = await this.taskUtils.sendMail();
        res.status(200).send(result);
    }
}




